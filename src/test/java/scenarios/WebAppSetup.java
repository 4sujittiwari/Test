package scenarios;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebAppSetup {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "E://workspace//Test//Drivers//chromedriver_win.exe");

		// Initialize browser
		WebDriver driver = new ChromeDriver();

		// Open facebook
		driver.get("http://www.facebook.com");
	}
}
